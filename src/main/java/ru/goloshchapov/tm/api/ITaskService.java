package ru.goloshchapov.tm.api;

import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add (String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task updateOneById(String id, String name, String description);

    Task updateOneByIndex(Integer index, String name, String description);

    Task startTaskById(String id);

    Task startTaskByIndex(Integer index);

    Task startTaskByName(String name);

    Task finishTaskById(String id);

    Task finishTaskByIndex(Integer index);

    Task finishTaskByName(String name);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByName(String name, Status status);

    Task changeTaskStatusByIndex(int index, Status status);

}
