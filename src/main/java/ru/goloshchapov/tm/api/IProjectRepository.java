package ru.goloshchapov.tm.api;

import ru.goloshchapov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    int size();

    void add(Project project);

    void remove(Project project);

    void clear();

    Project findOneById(String id);

    Project removeOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project startProjectById(String id);

    Project startProjectByIndex(Integer index);

    Project startProjectByName(String name);

    Project finishProjectById(String id);

    Project finishProjectByIndex(Integer index);

    Project finishProjectByName(String name);

}
