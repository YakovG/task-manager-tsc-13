package ru.goloshchapov.tm.api;

import ru.goloshchapov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllByProjectId(String projectId);

    Task bindToProjectById(String taskId, String projectId);

    Task unbindFromProjectById(String taskId, String projectId);

    List<Task> removeAllByProjectId(String projectId);

}
