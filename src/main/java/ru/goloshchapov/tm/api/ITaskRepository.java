package ru.goloshchapov.tm.api;

import ru.goloshchapov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    int size();

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task startTaskById(String id);

    Task startTaskByIndex(Integer index);

    Task startTaskByName(String name);

    Task finishTaskById(String id);

    Task finishTaskByIndex(Integer index);

    Task finishTaskByName(String name);

    List<Task> findAllByProjectId(String projectId);

    Task bindToProjectById (String taskId, String projectId);

    Task unbindFromProjectById(String taskId);

    List<Task> removeAllByProjectId(String projectId);

}
