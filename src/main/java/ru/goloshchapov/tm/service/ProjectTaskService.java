package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.IProjectRepository;
import ru.goloshchapov.tm.api.IProjectTaskService;
import ru.goloshchapov.tm.api.ITaskRepository;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (isEmpty(projectId)) return null;
        if (projectRepository.findOneById(projectId) == null) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindToProjectById(final String taskId, final String projectId) {
        if (isEmpty(taskId) || isEmpty(projectId)) return null;
        if (projectRepository.findOneById(projectId) == null) return null;
        return taskRepository.bindToProjectById(taskId, projectId);
    }

    @Override
    public Task unbindFromProjectById(final String taskId, final String projectId) {
        if (isEmpty(taskId) || isEmpty(projectId)) return null;
        if (projectRepository.findOneById(projectId) == null) return null;
        return taskRepository.unbindFromProjectById(taskId);
    }

    @Override
    public List<Task> removeAllByProjectId(final String projectId) {
        if (isEmpty(projectId)) return null;
        return taskRepository.removeAllByProjectId(projectId);
    }

}
