package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.ITaskRepository;
import ru.goloshchapov.tm.api.ITaskService;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.checkIndex;
import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task add(final String name, final String description) {
        if (isEmpty(name) || isEmpty(description)) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneById(final String id) {
        if (isEmpty(id)) return null;
        return taskRepository.findOneById(id);
    }

    @Override
    public Task removeOneById(final String id) {
        if (isEmpty(id)) return null;
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        final int size = taskRepository.size();
        if (checkIndex(index,size)) return taskRepository.findOneByIndex(index);
        else return null;
    }

    @Override
    public Task findOneByName(final String name) {
        if (isEmpty(name)) return null;
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final int size = taskRepository.size();
        if (checkIndex(index,size)) return taskRepository.removeOneByIndex(index);
        else return null;
    }

    @Override
    public Task removeOneByName(final String name) {
        if (isEmpty(name)) return null;
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task updateOneById(final String id, final String name, final String description) {
        if (isEmpty(id) || isEmpty(name)) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(name);
        return task;
    }

    @Override
    public Task updateOneByIndex(final Integer index, final String name, final String description) {
        final int size = taskRepository.size();
        if (!checkIndex(index,size)) return null;
        if (isEmpty(name)) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final String id) {
        if (isEmpty(id)) return null;
        return taskRepository.startTaskById(id);
    }

    @Override
    public Task startTaskByIndex(final Integer index) {
        final int size = taskRepository.size();
        if (checkIndex(index,size)) return taskRepository.startTaskByIndex(index);
        else return null;
    }

    @Override
    public Task startTaskByName(final String name) {
        if (isEmpty(name)) return null;
        return taskRepository.startTaskByName(name);
    }

    @Override
    public Task finishTaskById(final String id) {
        if (isEmpty(id)) return null;
        return taskRepository.finishTaskById(id);
    }

    @Override
    public Task finishTaskByIndex(final Integer index) {
        final int size = taskRepository.size();
        if (checkIndex(index,size)) return taskRepository.finishTaskByIndex(index);
        else return null;
    }

    @Override
    public Task finishTaskByName(final String name) {
        if (isEmpty(name)) return null;
        return taskRepository.finishTaskByName(name);
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (isEmpty(id)) return null;
        final Task task = findOneById(id);
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(final String name, final Status status) {
        if (isEmpty(name)) return null;
        final Task task = findOneByName(name);
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final int index, final Status status) {
        final int size = taskRepository.size();
        if (!checkIndex(index, size)) return null;
        final Task task = findOneByIndex(index);
        task.setStatus(status);
        return task;
    }

}
